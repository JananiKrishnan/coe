<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <div class="custom_footer">
            <div class="footer_links"><a href="#">
              Home
          </a>
          | 
          <a href="#">
            About COE
          </a>
		  </div>
          <h4>
            &copy; 2014 Payoda Inc. All rights reserved. 
          </h4>
          </div>
          <div id="charms" class="win-ui-dark slide">
            
          </div>
          
          <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
          <!--[if IE 7]>
<script type="text/javascript" src="scripts/bootmetro-icons-ie7.js">
<![endif]-->
          
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js">
          </script>
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootmetro-panorama.js">
          </script>
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootmetro-pivot.js">
          </script>
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootmetro-charms.js">
          </script>
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap-datepicker.js">
          </script>
          
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.mousewheel.min.js">
          </script>
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.touchSwipe.min.js">
          </script>
          
          <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/holder.js">
          </script>
          <!--
<script type="text/javascript" src="assets/js/perfect-scrollbar.with-mousewheel.min.js">
</script>
-->
          
          <script type="text/javascript">
            
            $('.panorama').panorama({
              //nicescroll: false,
              showscrollbuttons: true,
              keyboard: true,
              parallax: true
            }
                                   );
          
          //      $(".panorama").perfectScrollbar();
          
          $('#pivot').pivot();
          
          </script>
          <?php wp_footer(); ?>
</body>
</html>
