<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*Function to redirect to a specific page on login*/

add_action( 'user_login_check', 'user_redirect_login' );
function user_redirect_login() {
  if (!is_user_logged_in()) {
      $link = home_url();
      if(isset($_REQUEST['project_title']) or isset($_REQUEST['project_id']))
      {
          $link = $link.'?user_auth=failed';
      }
      wp_redirect($link);     
  }   
}
function admin_default_page() {
    return site_url() . '/landing';
}


/*
 * Fucntion to remove user bar only for users
 *
 */
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

/**
 * Login redirect method action
 */
add_filter('login_redirect', 'admin_default_page');

/*Function to redirect to home page on logout*/
function go_to_home() {
    wp_redirect(home_url());
    exit();
}

add_action('wp_logout', 'go_to_home');

/*Function to add widgets to admin page*/
if (function_exists('register_sidebar')) {

    register_sidebar(array(
        'name' => 'COE Who is New',
        'id' => 'who_is_new',
        'description' => 'Widget area for who is new in login page',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'COE RSS Feed',
        'id' => 'coe_rss_feed',
        'description' => 'Widget area for RSS Feed in login page',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'COE Lastest three projects',
        'id' => 'coe_lastest_three_projects',
        'description' => 'Widget area for lastest three projects in login page',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'COE User Project List Slider',
        'id' => 'coe_project_list_slider',
        'description' => 'Widget area for user project list in login landing page',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebar(array(
        'name' => 'COE Search bar',
        'id' => 'coe_search_bar',
        'description' => 'Widget area for search operation',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}

add_action( 'wp_login_failed', 'login_failure' );  // hook failed login

function login_failure( $username ) {
   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   $server = explode('?',$referrer);
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $server[0] . '?login=failure' );
       return;
   }
}
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'project',
		array(
			'labels' => array(
				'name' => __( 'Project' ),
				'singular_name' => __( 'Project' )
			),
                     'supports' => array( 'title', 'editor','thumbnail' ),
		'public' => true,
		'has_archive' => true,
		)
	);
}
add_theme_support('post-thumbnails');
/* To add roles to WP */

add_role("Developer", "Developer");
add_role("Dev_admin", "Dev_admin");
add_role("QA", "QA");
add_role("QA_admin", "QA_admin");
add_role("BA", "BA");
add_role("BA_admin", "BA_admin");