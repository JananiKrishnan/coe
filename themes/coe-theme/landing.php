<?php
/*
  Template Name:Landing
 */
do_action('user_login_check');
get_header();
$username = wp_get_current_user();
$coeObject = new COECustom();
$project_list = $coeObject->get_user_project_list($username->user_login);

$user_name = wp_get_current_user();
global $current_user, $wpdb;
$role = $wpdb->prefix . 'capabilities';
$current_user->role = array_keys($current_user->$role);
$role = $current_user->role[0]; // Role from WP
$user_id = get_current_user_id(); //Current user id
$string = $user_name->user_login; //Current user name

$key = 'job_title';
$user_role_array = get_user_meta($user_id, $key);
$user_role = $user_role_array[0]; //Curent user role from AD
/* Roles in AD listed in arrays as per need */
$dev = array("Trainee", "Software Engineer I", "Software Engineer II", "Software Engineer III", "Visual Designer I", "Visual Designer II", "Visual Designer III");
$dev_admin = array("Project Specialist I", "Project Specialist II", "Project Specialist III", "Senior Project Specialist I", "Senior Project Specialist II", "Senior Project Specialist III", "Senior Project Partner II", "Associate Project Partner I", "UX Architect I", "UX Architect II", "UX Architect III");
$qa = array("Test Engineer I", "Test Engineer II", "Test Engineer III");
$qa_admin = array("Senior Test Enginner I", "Senior Test Engineer II", "Senior Test Engineer III");
$ba = array("Business Analyst I", "Business Analyst II", "Business Analyst III");
$ba_admin = array("Associate Client Partner I", "Associate Client Partner II", "Associate Client Partner III", "Client Partner I", "Client Partner II", "Client Partner III");
if($string=='admin')
{
    $setrole = 'administrator';
}

if($string=='coe_admin')
{
    $setrole = 'administrator';
}


 if($string=='coe_admin')
{
    $setrole = 'administrator';
} 

if (in_array($user_role, $dev)) {
    $setrole = 'Developer';
}
if (in_array($user_role, $dev_admin)) {
     $setrole = 'Dev_admin';
}
if (in_array($user_role, $qa)) {
    $setrole = 'QA';
}
if (in_array($user_role, $qa_admin)) {
    $setrole = 'QA_admin';
}
if (in_array($user_role, $ba)) {
    $setrole = 'BA';
}
if (in_array($user_role, $ba)) {
     $setrole = 'BA_admin';
}
$userrole = new WP_User($user_name);
$urole = $userrole->set_role($setrole);

?>
<div style="width:100%;">
    <div style="width:100%;height:150px;">
        <script src="<?php echo get_template_directory_uri(); ?>/assets/jsCarousel/jsCarousel.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/projectcontent/js/jquery.content-panel-switcher.js" type="text/javascript"></script>
        <link href="<?php echo get_template_directory_uri(); ?>/assets/projectcontent/style/jquery.content-panel-switcher.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/assets/jsCarousel/jsCarousel.css" rel="stylesheet" type="text/css" />
        <blockquote  style="background:#339933;color:#FFF;width:30%;">My Project's</blockquote>
<?php
if (count($project_list) > 0) {
    ?>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#jsCarousel').jsCarousel(
                            {
                                onthumbnailclick: function(src) {
                                },
                                autoscroll: false});
                    $(document).ready(function() {
                        jcps.fader(300, '#switcher-panel');
                    });
                   $(document).ready(function() {
  $("img").click(function() {
    $(this).css('border', "solid 4px red");  
  });
});
                });
            </script>   
            <div id="jsCarousel">

    <?php
    
    //echo $idd;
   // echo $key;
    $count = 0;
    foreach ($project_list as $key => $project) {
  //$class = ($count == 0) ? 'active' : 'inactive';
  //echo $class;
       if ($count == 0) {
            $num = $key;
            $prname = $project;
        }
        $count++;
         $idd = $_GET[id];
       //  echo $idd;
         if($key==$idd)
         {
             $class="active";
         }
             else 
             {
                 $class="";
         }
        ?>

                    <div id="<?php echo $key; ?>" class="projects_name <?php echo "thumbnail-" .$class; ?>" >
                        <a href="<?php echo site_url(); ?>/landing?id=<?php echo $key; ?>">  
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/jsCarousel/images/img_1.jpg" /></a><br />
                        <span class="thumbnail-text"><?php echo $project; ?></span>
                    </div>

        <?php
    }
    
   
    ?>                        
            </div>
                <?php
            } else {
                ?>
            <div>
                <br> 
                <center><h1>You are not assigned to any projects.</h1></center>
            </div>
        <?php }
        ?>
    </div>
    <br />
    <br />
    <br />
    <div style="float:left;width:75%;height:400px;margin-left: 10px;">
        <center><h2 style="border-bottom: solid #339933;border-top: solid #339933;padding:5px;">
<?php
$a = get_permalink();
$b = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if ($a == $b) {
    echo $prname;
    echo "<br>";
       
} else {
    $p = get_post($idd);
    $title = $p->post_title;
    echo $title;

}
?></h2></center>
        <div class="block wide margin_auto" >
            <div id="switcher-panel" style='height:200px'></div>

            <!-- Dummy Data -->
            <div id="home-content" class="switcher-content show" >
<!--             <img style="float:left;padding:10px;" src="<?php echo get_template_directory_uri(); ?>/assets/jsCarousel/images/img_2.jpg" />  -->
 <?php echo get_the_post_thumbnail($key); ?>
                <p style="text-align:justify">
                    <center>
<?php
$a = get_permalink();
$b = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if ($a == $b) {
    $post = get_post($num);
    echo $post->post_content;
    $ID = $post->ID;
} else {
    echo $p->post_content;
    $ID = $p->ID;
}
//added by SLP - code starts - retrieving data for tech stalk, urls, team members
$meta_values = get_post_meta($ID, 'price_data', true);
$post_data_dev_url = simple_fields_get_post_group_values($ID, 'dev urls', true, 2);
$post_data_qa_url = simple_fields_get_post_group_values($ID, 'qa urls', true, 2);
$post_data_live_url = simple_fields_get_post_group_values($ID, 'Live urls', true, 2);
$post_data_svn_url = simple_fields_get_post_group_values($ID, 'svn urls', true, 2);
$post_data_server_vault = simple_fields_get_post_group_values($ID, 'server vault', true, 2);
$proj_image = get_post_meta($ID, 'image', true);
//print_r($proj_image); die();
$project_det = get_post_meta($ID);
$proj_name = $project_det[project_name][0];
$team_mem = $coeObject->get_project_team_members($proj_name);
//added by SLP - code ends
?>
                         </center>
                </p>
            </div>   

                    <?php
//added by SLP - code starts
                    if ($ID) {
                        ?>
                <div id="photography-content" class="switcher-content">
                    <div class="horiz_ul" style="overflow-x:hidden;overflow-y:hidden">
                        <ul>
                <?php if ($meta_values) { ?>
                                <li><table style="width:200px;float:left;margin:10px"class="table table-condensed">
                                        <tr class="success">
                                            <td>
                                                OS Name : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[os][name]);
        ?>
                                            </td>                              
                                        </tr>
                                        <tr class="success">
                                            <td>
                                                Version : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[os][version]);
        ?>
                                            </td>                              
                                        </tr>
                                        <tr class="success">
                                            <td>
                                                Type : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[os][type]);
        ?>
                                            </td>                              
                                        </tr>
                                        <tr class="success">
                                            <td>
                                                Server : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[server]);
        ?>
                                            </td>                              
                                        </tr>
                                        <tr class="success">
                                            <td>
                                                Database : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[db]);
        ?>
                                            </td>                              
                                        </tr>
                                        <tr class="success">
                                            <td>
                                                Language  : 
                                            </td>
                                            <td>
        <?php
        print ($meta_values[language]);
        ?>
                                            </td>                              
                                        </tr>
                                    </table></li>  
                                                <?php
                                            } else {
                                                ?>
                                <div>
       
                                    <br> 
                                    <h1>Technology stack details not yet added.</h1>
                                </div>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>

                <div id="design-content" class="switcher-content">
                    <div class="horiz_ul" style="overflow-x:hidden;overflow-y:hidden">
                        <ul>
                            <li>
                                <h3>QA Url's</h3>
                                <table style="width:300px;float:left;margin:10px"class="table table-condensed"> 
    <?php
    $count = 0;
    if ($post_data_qa_url) {
        foreach ($post_data_qa_url as $data_qa_url) {
            if (strlen($data_qa_url[qa_url]) > 0) {
                ?> 
                                                <tr class="success">
                                                    <td><?php echo $count + 1; ?></td>
                                                    <td><?php echo $data_qa_url[qa_url]; ?></td>
                                                </tr>
                                                <?php
                                            } else {
                                                ?>
                                                <tr class="success">
                                                    <td><?php echo "QA details not yet added."; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $count++;
                                        }
                                    } else {
                                        ?>
                                        <tr class="success">
                                            <td><?php echo "QA details not yet added."; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>

                            <li> 
                                <h3>Dev Url's</h3>
                                <table style="width:300px;float:left;margin:10px"class="table table-condensed">                                
                                    <?php
                                    $count = 0;
                                    if ($post_data_dev_url) {
                                        foreach ($post_data_dev_url as $data_dev_url) {
                                            if (strlen($data_qa_url[qa_url]) > 0) {
                                                ?>                                       
                                                <tr class="success">
                                                    <td><?php echo $count + 1; ?></td>
                                                    <td><?php echo $data_dev_url[dev_url]; ?></td>
                                                </tr>
                                                <?php
                                            } else {
                                                ?>
                                                <tr class="success">
                                                    <td><?php echo "Dev details not yet added."; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $count++;
                                        }
                                    } else {
                                        ?>
                                        <tr class="success">
                                            <td><?php echo "Dev details not yet added."; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
<?php  if(($role=="Dev_admin") || ($role=="QA_admin")||($role=="BA_admin"))
                                    { ?>
                            <li>
                                <h3>Live Url's</h3>
                                <table style="width:300px;float:left;margin:10px"class="table table-condensed">                                
                                    <?php
                                   
                                    $count = 0;
                                    if ($post_data_live_url) {
                                        foreach ($post_data_live_url as $data_live_url) {
                                            if (strlen($data_live_url[live_url]) > 0) {
                                                ?>  
                                                <tr class="success">
                                                    <td><?php echo $count + 1; ?></td>
                                                    <td><?php echo $data_live_url[live_url]; ?></td>
                                                </tr> 
                                        <?php }  else { ?>
                                                <tr class="success">
                                                    <td><?php echo "Live url details not yet added."; ?></td>
                                                </tr> 
                <?php
            }
            $count++;
        }
    } else {
        ?>
                                        <tr class="success">
                                            <td><?php echo "Live url details not yet added."; ?></td>
                                        </tr>  
                                    <?php } }?>
                                </table>
                            </li>

                            <li>
                                <h3>SVN Url's</h3>
                                <table style="width:300px;float:left;margin:10px"class="table table-condensed">                                
                                    <?php
                                    $count = 0;
                                    if ($post_data_svn_url) {
                                        foreach ($post_data_svn_url as $data_svn_url) {
                                            if (strlen($data_svn_url[svn_url]) > 0) {
                                                ?> 
                                                <tr class="success">
                                                    <td><?php echo $count + 1; ?></td>
                                                    <td><?php echo $data_svn_url[svn_url]; ?></td>
                                                </tr> 

                                            <?php } else {
                                                ?>
                                                <tr class="success">
                                                    <td><?php echo "SVN url details not yet added."; ?></td>
                                                </tr>   
                <?php
            } $count++;
        }
    } else {
        ?>
                                        <tr class="success">
                                            <td><?php echo "SVN url details not yet added."; ?></td>
                                        </tr>  
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="music-content" class="switcher-content">
                    <div class="horiz_ul" style="overflow-x:hidden;overflow-y:hidden">
                        <ul>
    <?php
    $count = 0;
    if ($post_data_server_vault) {
        foreach ($post_data_server_vault as $data_server_vault) {
            ?>
                                    <li>
                                        <table style="width:300px;float:left;margin:10px"class="table table-condensed">
                                            <tr class="success">
                                                <td>Name</td>
                                                <td><?php echo $data_server_vault[name]; ?></td>
                                            </tr><tr class="success">
                                                <td>Server IP</td>
                                                <td><?php echo $data_server_vault[ip_address]; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <td>Port</td>
                                                <td><?php echo $data_server_vault[port]; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <td>Username</td>
                                                <td><?php echo $data_server_vault[username]; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <td>Password</td>
                                                <td><?php echo $data_server_vault[Password]; ?></td>
                                            </tr>
            <?php
        }
    } else {
        ?>                                        <tr class="success">
                                            <td><?php echo "Server vault details not yet added."; ?></td>
                                             </tr> 
                                    <?php } ?>                                        
                                </table></li>
                        </ul>
                    </div>
                </div>

                <div id="about-content" class="switcher-content">
                    <h3>Other related Documents</h3> 
                </div>
    <?php
    //added by SLP - code ends
}
?>
            <br />
            <div class="header">
                <center>
                    <div class="nav_buttons pagination"> 
                        <ul>
                            <li><a id="home" class="switcher"><b>About</b>  </a> </li>
                            
                            <li><a id="photography" class="switcher"><b>Technology Stack</b></a></li>
                            <li><a id="design" class="switcher"><b>URL's</b></a> </li>
                            <?php if(($role=="Dev_admin")||($role=="QA_admin")||($role=="BA_admin"))
                            {?>
                            <li><a id="music" class="switcher"><b>Sever Vault</b></a></li>
                            <?php } ?>
                            <li><a id="about" class="switcher"><b>Others</b></a></li>
                        </ul>
                    </div> 
            </div>
        </div>
    </div>
 <?php 
    $loggedin_user = $coeObject->get_user_det($username->user_login);      
        $loggedin_name =$loggedin_user[0][displayname][0]; 
        //print $loggedin_name;
        //die(); ?>
    <!-- added by slp for displaying team members -->
    <div class="team_members_list_box">
        <div style="height:90%;overflow-y: scroll;overflow-x:hidden;padding: 10px;">
            <center><h3>Project Owner</h3></center>
            <!-- <p style="border-bottom:solid #339933;border-width:1px;">Team Member 1</p>  -->          

            <center><h3>Development Team</h3></center>
            <?php if ($team_mem['dev']) {
            foreach ($team_mem['dev'] as $team['dev']) { 
                if ($loggedin_name==$team['dev']){
                    $class="active";
                    }
                    else {
                        $class="";
                    }?>                    
            <div class="<?php echo $class; ?>"><p style="border-bottom:solid #339933;border-width:1px;"><?php echo $team['dev']; ?></p></div>
            <?php  }
            } ?>

            <center><h3>Business Analyst</h3></center>
        <?php if ($team_mem['ba']) {
                foreach ($team_mem['ba'] as $team['ba']) { 
                    if ($loggedin_name==$team['ba']){
                    $class="active";
                    }
                    else {
                        $class="";
                    }?>
            <div class="<?php echo $class; ?>"><p style="border-bottom:solid #339933;border-width:1px;"><?php echo $team['ba']; ?></p></div>
            <?php }
            } ?>   

           
                        <center><h3>Testing Team</h3></center> 
            <?php if ($team_mem['qa']) {
                foreach ($team_mem['qa'] as $team['qa']) { 
                    if ($loggedin_name==$team['qa']){
                    $class="active";
                    }
                    else {
                        $class="";
                    }?>
            <div class="<?php echo $class; ?>"><p style="border-bottom:solid #339933;border-width:1px;"><?php echo $team['qa']; ?></p></div>
            <?php }
            } ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
