<?php
/*
Demo: AJAX Search Suggest (WeAreHunted.com Style)
Version 1.0
Author: Ian Lunn
Author URL: http://www.ianlunn.co.uk/
Demo URL: http://www.ianlunn.co.uk/demos/ajax-search-suggest-wearehunted/
Tutorial URL: http://www.ianlunn.co.uk/blog/code-tutorials/ajax-search-suggest-wearehunted/
GitHub: https://github.com/IanLunn/AJAX-Search-Suggest--WeAreHunted.com-Style-/

Dual licensed under the MIT and GPL licenses:
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/gpl.html
*/

include ("test-data.php");
//include the array that contains our example search results (in the real world, you'd connect to and query a database)
include(fs_get_wp_config_path());
if(isset($_POST['latestQuery'])){ //if the auto-suggest script receives a query...
     $args = array('s' => $_POST['latestQuery']);
     $search = new WP_Query($args);
      $result = array(); //set up an array that we'll store the matched search terms in (and finally send back to the JavaScript)
	foreach($search->posts as $key => $post){ //for each value in the data array...
				$result[$key]['title'] = $post->post_title;
                                $noimg=get_template_directory_uri().'/assets/img/no_img.png';
                                $imgurl=get_field("project_image",$post->ID);
                                $result[$key]['imageurl']=($imgurl)?$imgurl:$noimg;
	}
	echo json_encode($result); //encode the results list as a JavaScript object, and send it back to the JavaScript
}

/*
 * Method to return wp_config.php basepath 
 */
function fs_get_wp_config_path()
{
    $base = dirname(__FILE__);
    $path = false;

    if (@file_exists(dirname(dirname($base))."/wp-config.php"))
    {
        $path = dirname(dirname($base))."/wp-config.php";
    }
    else
    if (@file_exists(dirname(dirname(dirname($base)))."/wp-config.php"))
    {
        $path = dirname(dirname(dirname($base)))."/wp-config.php";
    }
    else
    $path = false;

    if ($path != false)
    {
        $path = str_replace("\\", "/", $path);
    }
    return $path;
}
?>