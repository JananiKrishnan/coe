<!DOCTYPE html>

<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
        <meta charset="utf-8" />
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <!-- Mobile viewport optimized: h5bp.com/viewport -->
        <meta name="viewport" content="width=device-width">

        <title>Payoda | COE</title>

        <meta name="robots" content="noindex, nofollow">
        <meta name="description" content="BootMetro : Simple and complete web UI framework to create web apps with Windows 8 Metro user interface." />
        <meta name="keywords" content="bootmetro, modern ui, modern-ui, metro, metroui, metro-ui, metro ui, windows 8, metro style, bootstrap, framework, web framework, css, html" />
        <meta name="author" content="AozoraLabs by Marcello Palmitessa"/>
        <!--SEARCH -->
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/google_fonts_1.css" rel="stylesheet" type="text/css">
        <!--END SEARCH -->
        <!-- remove or comment this line if you want to use the local fonts -->
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/google_fonts_2.css" rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootmetro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootmetro-responsive.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootmetro-icons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootmetro-ui-light.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/datepicker.css">

        <!--  these two css are to use only for documentation -->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/demo.css">

        <!-- Le fav and touch icons -->
        <link type="text/css" rel="shortcut icon" href="assets/ico/favicon.ico">
        <link type="text/css" rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/ico/apple-touch-icon-144-precomposed.png">
        <link type="text/css" rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/ico/apple-touch-icon-114-precomposed.png">
        <link type="text/css" rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/ico/apple-touch-icon-72-precomposed.png">
        <link type="text/css" rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/ico/apple-touch-icon-57-precomposed.png">

        <!-- All JavaScript at the bottom, except for Modernizr and Respond.
    Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
    For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr-2.6.2.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>
            window.jQuery || document.write("<script src='<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.8.3.min.js'>\x3C/script>")
        </script>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-3182578-6']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            }
            )();
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
    <p class="chromeframe">
    You are using an 
    <strong>
    outdated
    </strong>
    browser. Please 
    <a href="http://browsehappy.com/">
    upgrade your browser
    </a>
    or 
    <a href="http://www.google.com/chromeframe/?redirect=true">
    activate Google Chrome Frame
    </a>
    to improve your experience.
    </p>
    <![endif]-->

        <!-- Direction Navigator Skin Begin -->
        <!--Search Wrapper  -->
        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('coe_search_bar')) : else : ?>  


        <?php endif; ?> 

        <div id="wrap">

            <!-- Header
    ================================================== -->
            <div id="nav-bar" style="height:90px">
                <div class="pull-left">
                    <div id="header-container">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/payoda.png" title="payoda" />
                        <h2>
                            <b>Center of Excellence</b>
                        </h2>
                    </div>
                </div>
                <div class="pull-right" style="margin-right:5%">
                    <?php
                    if (is_user_logged_in()) {
                        $user_name = wp_get_current_user();
                        ?>
                        <div class="dropdown pull-left">
                            <div class="pull-left">
                                <h3><?php echo $user_name->user_login; ?></h3>
                            </div>
                            <a  class="win-command pull-right" data-toggle="dropdown" href="#"><span class="win-commandicon win-commandring icon-user"></span></a>
                            <ul class="dropdown-menu">
                              <!--   <li><a href="#">My Projects</a></li>
                                <li><a href="./hub.html">View Profile</a></li>
                                <li><a href="./tiles-templates.html">Sever Vault</a></li>
                                <li class="divider"></li> -->
                                <li><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
                            </ul>
                        </div>
                    <?php } else {
                        ?>
                        <!--Login form-->
                        <div class="pull-left">
                            <button type="button" class="login_button pull-right" style='margin:5px;'>
                                <span>
                                    <span class="icon-login">
                                    </span>
                                </span>
                            </button>

                            <form id="loginform" method="post" class="form-horizontal pull-left" name="loginForm" action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" style='margin:5px;' >
                                <div>
                                    <style>
                                        #alerts-container {
                                            position: fixed;
                                            top: 5px;
                                            right: 5px;
                                            padding: 0;
                                            z-index: 1100;
                                            width: 100%;
                                        }
                                        .alert-success,.alert-info {
                                            font-size: 10px;
                                            padding:0px;
                                            width:98%;
                                            margin-left:1%;
                                            padding:5px;
                                            opacity: .8;
                                        }
                                        #alerts-container .alert-success  {
                                            /* add a shadow */
                                            -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
                                            box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
                                        }
                                    </style>
                                    <?php
                                    if(isset($_REQUEST['login'])) {
                                    ?>
                                    <div id="alerts-container">
                                        <div class="alert alert-success" style="float: left;height:20px;">
                                            <button type="button" class="close" data-dismiss="alert"></button>  
                                            <center><b>Login Failed!</b> Wrong User credentials</center>
                                        </div>
                                    </div>
                               <?php
                                    }
                               ?>
                                     <?php
                                    if(isset($_REQUEST['user_auth'])) {
                                    ?>
                                    <div id="alerts-container">
                                        <div class="alert alert-info" style="float: left;height:20px;">
                                            <button type="button" class="close" data-dismiss="alert"></button>  
                                            <center>User need to <b>Login</b> !!! </center>
                                        </div>
                                    </div>
                               <?php
                                    }
                               ?>
                                    <span id="coelogin_content" style="display:none;">
                                        <div class="control-group">
                                            <div class="controls input-prepend">
                                                <span class="add-on">
                                                    <span class="icon-user"></span>
                                                </span>
                                                <input type="text" id="inputEmail" name="log" placeholder="Username" class="input-xlarge" required>
                                            </div>
                                        </div>
                                        <div class="control-group pull-left" style="width:200px">
                                            <div class="controls input-prepend">
                                                <span class="add-on">
                                                    <span class="icon-star">
                                                    </span>
                                                </span>
                                                <input type="password" id="inputPassword" name="pwd" class="input-medium" placeholder="Password" required>
                                            </div>
                                        </div>
                                        <div class="control-group pull-right">
                                            <div class="controls">
                                                <button id="loginButton" type="submit" value="Sign In" class="btn input-small" />Sign in</button>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </form>
                            <script>
                                $(document).ready(function() {
                                    $(".login_button").click(function() {
                                        $(".login_button").html('<span class=" icon-cross"></span>');
                                        $("#coelogin_content").toggle("slow", function() {
                                            var display = $('#coelogin_content').css('display');
                                            if (display == 'inline' || display == 'inline-block')
                                            {
                                                $(".login_button").html('<span class=" icon-cross"></span>');
                                            }
                                            else
                                            {
                                                $(".login_button").html('<span class="icon-login"></span>');
                                            }
                                        });
                                    });
                                });
                            </script>
                        </div>
                    <?php } ?>
                    <!--Search button-->
                    <div id="top-info" class="pull-right">
                        <a id="search" href="#" class="win-command pull-right">
                            <span class="win-commandicon win-commandring icon-search-2">
                            </span>
                        </a>
                    </div>
                </div>
            </div>