<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>

<div class="metro panorama">
    <div class="pull-left" style="width:69%;height:80%;">
        <!-- Lastest three Projects-->
        <div class="pull-left" style="width:100%;height:300px;">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('coe_lastest_three_projects')) : else : ?>  
                 <h3>No Projects created</h3>
                <?php endif; ?>
            
          
        </div>
    </div>


    <div class="pull-left" style="width:25%;height:80%;margin-left:2%;">
        <div class="pull-left" style="width:100%;height:12%;">
<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('who_is_new')) : else : ?>  
                <h3>Welcome to Payoda!</h3>
            </div>
<?php endif; ?> 
    </div>
    <div class="pull-left" style="width:100%;height:70%;margin-top:8%">
        <blockquote class="pull-right" style="background:#339933;color:#FFF">What's new
        </blockquote>
        <div class="update_view">
            <div class="listview-item bg-color-blue pull-right" style="width:90%;">
                <div class="pull-left" href="#">
                    <img class="listview-item-object" data-src="holder.js/60x60">
                </div>
                <div class="listview-item-body">
                    <h4 class="listview-item-heading">
                        Feed Title
                    </h4>
                    <p class="two-lines">
                        Feed Description
                    </p>
                </div>
            </div>

            <div class="listview-item bg-color-green pull-right" style="width:90%">
                <div class="pull-left" href="#">
                    <img class="listview-item-object" data-src="holder.js/60x60">
                </div>
                <div class="listview-item-body">
                    <h4 class="listview-item-heading">
                        Feed Title
                    </h4>
                    <p class="two-lines">
                        Feed Description
                    </p>
                </div>
            </div>
            <div class="listview-item bg-color-orange pull-right" style="width:90%">
                <div class="pull-left" href="#">
                    <img class="listview-item-object" data-src="holder.js/60x60">
                </div>
                <div class="listview-item-body">
                    <h4 class="listview-item-heading">
                        Feed Title
                    </h4>
                    <p class="two-lines">
                        Feed Description
                    </p>
                </div>
            </div>
        </div>
        <div class="update_view">
            <div class="listview-item bg-color-grayLight pull-right" style="width:90%">
                <div class="pull-left" href="#">
                    <img class="listview-item-object" data-src="holder.js/60x60">
                </div>
                <div class="listview-item-body">
                    <h4 class="listview-item-heading">
                        Feed Title
                    </h4>
                    <p class="two-lines">
                        Feed Description
                    </p>
                </div>
            </div>

            <div class="listview-item bg-color-blue pull-right" style="width:90%">
                <div class="pull-left" href="#">
                    <img class="listview-item-object" data-src="holder.js/60x60">
                </div>
                <div class="listview-item-body">
                    <h4 class="listview-item-heading">
                        Feed Title
                    </h4>
                    <p class="two-lines">
                        Feed Description
                    </p>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/quoteslider/jquery.quovolver.js">
        </script>

        <!-- JavaScript Test Zone -->
        <script type="text/javascript">
            $(document).ready(function() {

                $('.update_view').quovolver();

            }
            );
        </script>

    </div>
</div>

</div>    


<?php
get_footer();
