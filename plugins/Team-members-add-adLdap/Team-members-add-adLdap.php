 <?php  
/*  
Plugin Name: Team Member ADD using adLdap   
Plugin URI: http://localhost/wordpress/  
Description: This plugin will check the members in Active directory and if correct map against the member to project.  
Version: 0.1  
Author: Muhil  
*/  	if (!class_exists('adLDAP')) {
				require_once('adldap/adLDAP.php');
			}
    
   class custom_ldap extends  adLDAP {
	   
	public function user_check($username) {
	
				$domain_controllers='74.63.187.138';
				$port='389';
				$base_dn='dc = dizziontestad,dc=vdi';
				$conn = ldap_connect($domain_controllers, $port );
				$ldaprdn='administrator@dizziontestad.vdi';
				$ldappass='D1zziOn!';
				ldap_set_option($conn, LDAP_OPT_REFERRALS, 0); 
				ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
				$bind=@ldap_bind($conn, $ldaprdn, $ldappass);
					if (!$bind){ echo -1; return; }
					
						$search_filter = "(&(objectCategory=person)(sAMAccountName=$username))";
						$attributes = array("displayname", "mail","samaccountname");
						$result = ldap_search($conn, $base_dn, $search_filter,$attributes);
						$entries = ldap_get_entries($conn, $result);
					
							if (isset($entries[0])) {
								echo 0;
							}
							else
							{
								echo 1;
							}
						}	
					}			
	  
		class team_member_list extends custom_ldap{
		function check_username_AD() {		
					if ( isset($_REQUEST) ) {
						$username = $_REQUEST['username'];
						$class_methods = get_class_methods($ADI = new team_member_list());
						$result = $ADI->user_check($username);
					}
				die();
			}
        }
		add_action( 'load-post.php', 'team_member_add_setup' );
		add_action( 'load-post-new.php', 'team_member_add_setup' );
		add_action( 'wp_ajax_check_team_member', array('team_member_list','check_username_AD'));
		function team_member_add_setup() {
			add_action( 'add_meta_boxes', 'team_member_add_post_meta_boxes' );
			add_action( 'save_post', 'team_member_save_post_class_meta', 10, 2 );
		}
		
        function team_member_add_post_meta_boxes() {
                add_meta_box( 'team-member-meta-class', 'Add Team Members', 'team_member_post_class_meta_box', 'post', 'side', 'high' );
			     }
		function team_member_post_class_meta_box( $object, $box ) { ?>
			<?php wp_nonce_field( basename( __FILE__ ), 'team_member_post_class_nonce' ); ?>
			<p>
				<label for="team-member-post-class"><?php _e( "Team Member", 'Add Team members' ); ?></label>
				 <br />
				<input  class="widefat" type="text" style="width:200px" name="team-member-post-class" id ="adduser_box"  size="30" />
				<input type="button" class="button tagadd" id="adduser"  value="Add">
				<div style="clear:both"></div>
				<div id="userlist">
				<?php
				 $userlist_count=esc_attr( get_post_meta( $object->ID, 'team_member_post_class_userlist_count', true ) );
				    for($i=0;$i<$userlist_count;$i++)
					 {
					   $team_member_meta_key='team_member_id_'.$i;
					   $team_member=esc_attr( get_post_meta( $object->ID, $team_member_meta_key, true ) );
					   ?>
					   <span id="team-member-<?php echo $i; ?>"><a href="#" style="text-align:right;float:right;text-decoration:none" onclick="team_member_delete(<?php echo $i; ?>)" class="userdelbutton">X</a><p><?php echo $team_member ?></p><input type="hidden" value="<?php echo $team_member ?>" name="team-member-id-<?php echo $i; ?>" id="team-member-id-<?php echo $i; ?>" /> <hr /></span>
					   <?php
					 }
				?>
				</div>
			<?php 
			if(strlen($userlist_count)==0)
			 {
			?>			
			<input type="hidden" id="userlist_count" name="userlist_count" value="0" />
			 <?php
			 }
			else
             {
	        ?>
			<input type="hidden" id="userlist_count" name="userlist_count" value="<?php echo $userlist_count ?>" />
			<?php
			}			 
			 ?>
			</p>
			 <script>
				  jQuery(document).ready(function($) {
							jQuery('#adduser').click(function() {
					         var username = jQuery('#adduser_box').val();
							$.ajax({
							url: ajaxurl,
								data: {
									'action':'check_team_member',
									'username' : username
								},
							success:function(data) {
							console.log(data);
							t="#userlist span p:contains('"+username+"')";
							team_member_count=jQuery(t).size();
							if(parseInt(data)==-1)
							{
							 alert('Could not bind LDAP server');
							}
							else 
							{
							 if(parseInt(data)==1)
							   { 
							     alert('Invalid user');
							   }
							   else if(team_member_count!=0)
							   {
							    alert('user already added');
							   }
							  else
                                {	
									list_count=$('#userlist_count').val();
									jQuery( "#userlist" ).append( '<span id="team-member-'+list_count+'"><a href="#" style="text-align:right;float:right;text-decoration:none" onclick="team_member_delete('+list_count+')" class="userdelbutton">X</a><p>'+username+'</p><input type="hidden" value="'+username+'" id="team-member-id-'+list_count+'" name="team-member-id-'+list_count+'" /> <hr /></span>');
									list_count=parseInt(list_count)+1;
									$('#userlist_count').val(list_count);
									jQuery('#adduser_box').val('');
                                }	
}								
							},
							error: function(errorThrown){
							console.log(errorThrown);
						}
					});		
				  });
				
				});
				function team_member_delete(id)
				 {
				   //console.log('id'+id);
				   
				   var userlist_count=jQuery('#userlist_count').val();
				   html=''
				   list_count=0;
				   for(i=0;i<userlist_count;i++)
				    {	
					   if(i!=id)
					    {
					//	  console.log(i);
							var username=jQuery('#team-member-id-'+i).val();
						    html=html+'<span id="team-member-'+list_count+'"><a href="#" style="text-align:right;float:right;text-decoration:none" onclick="team_member_delete('+list_count+')" class="userdelbutton">X</a><p>'+username+'</p><input type="hidden" value="'+username+'" name="team-member-id-'+list_count+'" id="team-member-id-'+list_count+'" /> <hr /></span>';	
					       list_count++;
						}
					}
					//console.log('userlist_c'+userlist_count);
				    //console.log('userlist_c_a'+list_count);
					jQuery('#userlist_count').val(list_count);
					jQuery( "#userlist" ).html(html);
				  }	
				 
				</script>
			<?php }	
function team_member_save_post_class_meta( $post_id, $post ) {
	if ( !isset( $_POST['team_member_post_class_nonce'] ) || !wp_verify_nonce( $_POST['team_member_post_class_nonce'], basename( __FILE__ ) ) )
		return $post_id;
	$post_type = get_post_type_object( $post->post_type );
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;
	$new_meta_value = ( isset( $_POST['team-member-post-class'] ) ? sanitize_html_class( $_POST['team-member-post-class'] ) : '' );
	$new_meta_value_userlist_count = ( isset( $_POST['userlist_count'] ) ? sanitize_html_class( $_POST['userlist_count'] ) : '' );
	
	$meta_key = 'team_member_post_class';
	$meta_key_userlist_count = 'team_member_post_class_userlist_count';
	$meta_value = get_post_meta( $post_id, $meta_key, true );
	$meta_value_userlist_count = get_post_meta( $post_id, $meta_key_userlist_count, true );
	if ((strlen($meta_value_userlist_count)==0))
	{ 
		add_post_meta( $post_id, $meta_key_userlist_count, $new_meta_value_userlist_count, true );
	}	
	elseif ( $new_meta_value_userlist_count != $meta_value_userlist_count )
	{
		update_post_meta( $post_id, $meta_key_userlist_count, $new_meta_value_userlist_count );
	}	
	
	for($i=0;$i<$new_meta_value_userlist_count;$i++)
	  { 
	    
	    $team_member_id='team-member-id-'.$i; 
	    $team_member_name = ( isset( $_POST[$team_member_id] ) ? sanitize_html_class( $_POST[$team_member_id] ) : '' );
        $meta_key_team_member='team_member_id_'.$i;
        $meta_value_team_member = get_post_meta( $post_id, $meta_key_team_member, true );
		if((strlen($meta_value_team_member)==0))
		{
		  add_post_meta( $post_id, $meta_key_team_member,  $team_member_name, true );
		  
		}
		elseif ( $team_member_name != $meta_value_team_member  )
		{
				update_post_meta( $post_id, $meta_key_team_member, $team_member_name);
	    }
	  }
}
?>