<?php
/*
  Plugin Name: COE Custom Widgets
  Plugin URI: http://localhost/wordpress/
  Description: Custom Widgets for COE Theme
  Version: 0.1
  Author: Muhilvarnan
  Depends: Active Directory Integration,COE Active directory integration
 */

/**
 * COE Active directory plugin integration
 * */
/**
 * Initialize the wordpress press widgets with multiple widget mode
 */
add_action('widgets_init', 'src_load_widgets');

/**
 * Load the widget class
 */
function src_load_widgets() {
    register_widget('Widget_Who_Is_New');
    register_widget('Widget_Project_Team_Members');
    register_widget('Widget_Search');
    register_widget('Widget_Latest_Three_Projects');
}

/**
 * Class for creating widget who is new 
 */
class Widget_Who_Is_New extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Who_Is_New', __('Who is New', 'wpb_widget_domain'),
                array('description' => __('Display top five members in Payoda Office', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $coeObject = new COECustom();
        $payoda_user = $coeObject->get_new_joinees();
        if (!empty($payoda_user)) {
            ?>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/newemp/css/component.css" />
            <div id="cbp-qtrotator" class="cbp-qtrotator" style="border-bottom:solid #339933">
                <?php
                for ($i = 0; $i < 5; $i++) {
                    ?>
                    <div class="cbp-qtcontent">
                        <blockquote>
                            <p>We welcome <b><?php echo $payoda_user[$i]->displayname; ?></b> to payoda family </p>
                            <footer>Designation | <?php echo $payoda_user[$i]->title; ?></footer>
                        </blockquote>
                    </div>
                    <?php
                }
                ?>
            </div>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/newemp/js/jquery.cbpQTRotator.min.js"></script>
            <script>
                $(function() {
                    $('#cbp-qtrotator').cbpQTRotator();
                });
            </script>
            <?php
        } else {
            ?>
            Welcome to payoda 
            <?php
        }
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget who is new 
 */
class Widget_Project_Team_Members extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Project_Team_Members', __('COE Project Team Members', 'wpb_widget_domain'), array('description' => __('Display Project team members', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $coeObject = new COECustom();
        $projectTeamMembers = $coeObject->get_project_team_members('PHP-Dizzion');
        ?>
        <div id="team-members">
            <?php
            foreach ($projectTeamMembers as $key => $member) {
                echo "<span class='team_member'>" . $member . "</span>";
            }
            ?>
        </div> 
        <?php
    }

    // Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget who is new 
 */
class Widget_Search extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Search', __('COE Search', 'wpb_widget_domain'), array('description' => __('Search projects,Payoda Members and Reuseable compenents in payoda MediaWiki', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        ?>
        <div id="search-overlay">
            <h2>
                Start Search
            </h2>
            <div id="close">
                X
            </div>
            <form>
                <input id="hidden-search" type="text" autocomplete="off" />

                <!--hidden input the user types into-->
                <input id="display-search" type="text"  autocomplete="off" readonly="readonly" />


                <!--mirrored input that shows the actual input value-->
            </form>

            <div id="results">
                <h2 class="artists">
                    Artists
                </h2>
                <ul id="artists">
                </ul>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/search/scripts/ajax-search-suggest.js"></script> 
        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget latest three projects
 */
class Widget_Latest_Three_Projects extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Latest_Three_Projects', __('Latest Three Projects', 'wpb_widget_domain'),
                array('description' => __('Display latest three projects in Payoda Office', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $args = array('posts_per_page' => 5, 'offset' => 1);
        $myposts = get_posts($args);
#echo "<pre>";print_r($myposts);exit;
        ?>

        <div class="pull-left" style="width:68%;height:300px;">
            <ul class="grid cs-style-3" style="float:left">
                <li>
                    <figure>
        <?php
        $imgurl = get_field("project_image", $myposts[0]->ID);
        if (!isset($imgurl) || $imgurl == "")
            $img = get_template_directory_uri() . '/assets/hovercaption/images/4.png';
        else {
            $img = $imgurl;
        }
        ?>
                        <img src=<?php echo $img ?> style="width:600px;height:300px" alt="img04">
                        <figcaption>

                            <h3><?php echo $myposts[0]->post_title ?></h3>

                        </figcaption>
                    </figure>
                </li>
            </ul>
        </div>
        <div style="float:left;width:20%;margin-left:0px;">
            <ul class="grid cs-style-5" style="float:left">
                <li>
                    <figure>
                        <?php
                        $imgurl1 = get_field("project_image", $myposts[1]->ID);
                        $imgurl2 = get_field("project_image", $myposts[2]->ID);
                        if (!isset($imgurl1) || $imgurl1 == "")
                            $img1 = get_template_directory_uri() . '/assets/hovercaption/images/4.png';
                        else {
                            $img1 = $imgurl1;
                        }
                        if (!isset($imgurl2) || $imgurl2 == "")
                            $img2 = get_template_directory_uri() . '/assets/hovercaption/images/1.png';
                        else {
                            $img2 = $imgurl2;
                        }
                        ?>
                        <img src="<?php echo $img1 ?> "style="width:300px;height:147px" alt="img04">
                        <figcaption>
                            <h3><?php echo $myposts[1]->post_title ?></h3>	
                        </figcaption>
                    </figure>
                </li>
                <li>
                    <figure>
                        <img src="<?php echo $img2 ?> " style="width:300px;height:147px" alt="img04">
                        <figcaption>
                            <h3><?php echo $myposts[2]->post_title ?></h3>

                        </figcaption>
                    </figure>
                </li>
            </ul>
        </div>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/captionhover/js/toucheffects.js"></script>

        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE ', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}
?>