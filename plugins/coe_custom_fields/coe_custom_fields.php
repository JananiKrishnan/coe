<?php
/*
  Plugin Name: COE Custom Fields
  Plugin URI: http://localhost/wordpress/
  Description: creates dynamic fields during creation of post
  Version: 0.1
  Author: Gobinath

 */

function Print_price_fileds($cnt, $p = null) {
    $a = isset($p['lab'])?$p['lab']:"";
    $b = isset($p['val'])?$p['val']:"";
return  <<<HTML
<li>
    <label>Label:</label>
    <input type="text" name="price_data[$cnt][lab]" size="10" value="$a"/>

    <label>Value:</label>
    <input type="text" name="price_data[$cnt][val]" size="10" value="$b"/>
    <span class="remove button">Remove</span>
</li>
HTML
;
}


//add custom field - price
add_action("add_meta_boxes", "object_init");

function object_init(){
  add_meta_box("price_meta_id", "Technology stack:","price_meta", "project", "normal", "low");

}

function price_meta(){
 global $post;

  $data = get_post_meta($post->ID,"price_data",true);
  echo '<div>';
  echo '<ul id="price_items">';
  $c = 0;
    if (count($data) > 0){
        $name=isset($data['os']['name'])?$data['os']['name']:"";
        $version=isset($data['os']['version'])?$data['os']['version']:"";
        $type=isset($data['os']['type'])?$data['os']['type']:"";
        $server=isset($data['server'])?$data['server']:"";
        $db=isset($data['db'])?$data['db']:"";
        $lang=isset($data['language'])?$data['language']:"";
   echo '<li> 
       <h3>OS</h3>
          <label>name</label>
    <input type="text" name="price_data[os][name]" size="10" value="'.$name.'"/>
                 <span class="remove button">Remove</span></li>
                  <li><label>Version</label>
    <input type="text" name="price_data[os][version]" size="10" value="'.$version.'"/>
                 <span class="remove button">Remove</span></li>
                  <li><label>Type</label>
    <input type="text" name="price_data[os][type]" size="10" value="'.$type.'"/>
                 <span class="remove button">Remove</span></li><hr/>
                  <label>Webserver</label>
    <input type="text" name="price_data[server]" size="10" value="'.$server.'"/>
                 <span class="remove button">Remove</span></li>
                 <li><label>DB</label>
   <input type="text" name="price_data[db]" size="10" value="'.$db.'"/>
                 <span class="remove button">Remove</span></li>
    <li><label>Language</label>
   <input type="text" name="price_data[language]" size="10" value="'.$lang.'"/>
                 <span class="remove button">Remove</span></li>';
 $p=(array)$data;
        for($i=0;$i<sizeof($p);$i++){
            if ((isset($p[$i]['lab'])&&isset($p[$i]['val']))&&($p[$i]['lab']!="")&&($p[$i]['val']!="")){
                echo Print_price_fileds($c,$p[$i]);
                $c = $c +1;
            }
          
        }

    }
    echo '</ul>';

    ?>
        <span id="here"></span>
        <span class="add button"><?php echo __('Add Fields'); ?></span>
        <script>
            var $ =jQuery.noConflict();
                $(document).ready(function() {
                var count = <?php echo $c - 1; ?>; // substract 1 from $c
                $(".add").click(function() {
                    count = count + 1;
                    //$('#price_items').append('<li><label>Nr :</label><input type="text" name="price_data[' + count + '][n]" size="10" value=""/><label>Description :</label><input type="text" name="price_data[' + count + '][d]" size="50" value=""/><label>Price :</label><input type="text" name="price_data[' + count + '][p]" size="20" value=""/><span class="remove">Remove</span></li>');
                   $('#price_items').append('<? echo implode('',explode("\n",Print_price_fileds('count'))); ?>'.replace(/count/g, count));
                    return false;
                });
                $(".remove").live('click', function() {
                    $(this).parent().remove();
                });
            });
        </script>
        <style>#price_items {list-style: none;}</style>
    <?php
    echo '</div>';
}


//Save product price
add_action('save_post', 'save_details');

function save_details($post_id){ 
global $post;


    // to prevent metadata or custom fields from disappearing... 
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
    return $post_id; 
    // OK, we're authenticated: we need to find and save the data
    if (isset($_POST['price_data'])){
        $data = $_POST['price_data'];
        update_post_meta($post_id,'price_data',$data);
    }else{
        delete_post_meta($post_id,'price_data');
    }
} 