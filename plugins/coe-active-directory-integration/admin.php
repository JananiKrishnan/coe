<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

add_action('admin_menu', 'coe_active_directory_setting');

function coe_active_directory_setting() {
    add_menu_page('COE adLdap', 'COE adLdap', 'administrator', 'coe_settings', 'coe_adldap_display_settings');
}

function coe_adldap_display_settings() {

    $base_dn = (get_option('base_dn') != '') ? get_option('base_dn') : '';

    $domain_controllers = (get_option('domain_controllers') != '') ? get_option('domain_controllers') : '';

    $ad_port = (get_option('ad_port') != '') ? get_option('ad_port') : '';

    $ad_username = (get_option('ad_username') != '') ? get_option('ad_username') : '';

    $ad_password = (get_option('ad_password') != '') ? get_option('ad_password') : '';


    $html = '</pre>
<div class="wrap"><form action="options.php" method="post" name="options">
<h2>COE Active directory settings</h2>
' . wp_nonce_field('update-options') . '
<table class="form-table" width="30%" cellpadding="10">
<tbody>
<tr>
<td  align="left">
 <label>Base Domain</label>
 </td><td>
 <input type="text" name="base_dn" id="base_dn" value="' . $base_dn . '" />
</tr>
<tr>
<td  align="left">
 <label>Domain Controllers</label>
  </td><td>
 <input type="text" name="domain_controllers" id="domain_controllers" value="' . $domain_controllers . '" /></td>
</tr>
<tr>
<td  align="left">
<label>Port</label>
 </td><td>
<input type="text" name="ad_port" id="ad_port" value="' . $ad_port . '" />
</td>
</tr>
<tr>
<td align="left">
 <label>Admin Username</label>
  </td><td>
 <input type="text" name="ad_username" id="ad_username" value="' . $ad_username . '" /></td>
</tr>
<td align="left">
 <label>Admin Password</label>
  </td><td>
 <input type="password" name="ad_password" id="ad_password" value="' . $ad_password . '" /></td>
</tr>
</tbody>
</table>
 <input type="hidden" name="action" value="update" />
 <input type="hidden" name="page_options" value="base_dn,domain_controllers,ad_port,ad_username,ad_password" />
 <input type="submit" name="Submit" value="Update" />
 </form>
 </div>
<pre>
';
    echo $html;
}

function get_adldap_config() {
    $config=1;
    $base_dn = (get_option('base_dn') != '') ? get_option('base_dn') : $config=0;

    $domain_controllers = (get_option('domain_controllers') != '') ? get_option('domain_controllers') : $config=0;

    $ad_port = (get_option('ad_port') != '') ? get_option('ad_port') : $config=0;

    $ad_username = (get_option('ad_username') != '') ? get_option('ad_username') :$config=0;

    $ad_password = (get_option('ad_password') != '') ? get_option('ad_password') : $config=0;

    if($config) {
    $config_array = array(
        'base_dn' => $base_dn,
        'domain_controllers' => $domain_controllers,
        'ad_port' => $ad_port,
        'ad_username' => $ad_username,
        'ad_password' => $ad_password
    );
    return $config_array;
   }
   return;
}
