<?php

/*
  Plugin Name: COE Active directory integration
  Plugin URI: http://localhost/wordpress/
  Description: COE active directory plugin integration
  Version: 0.1
  Author: Santhiya,MuhilVarnan
  Depends: Active Directory Integration
 * 
 */
require(plugin_dir_path(__FILE__) . 'admin.php');

/**
 * Class COECustom extens the ldap active directroy features
 */
class COECustom extends ADIntegrationPlugin {

    protected $base_dn;
    protected $domain_controllers;
    protected $ad_port;
    protected $ad_username;
    protected $ad_password;
    protected $connection;

    public function __construct() {

        parent::__construct();
        $coe_adldap = get_adldap_config();
        if (!$coe_adldap) {
            return;
        }
        try {
            $this->base_dn = $coe_adldap['base_dn'];
            $this->domain_controllers = explode(',', $coe_adldap['domain_controllers']);
            $this->ad_port = $coe_adldap['ad_port'];
            $this->ad_username = $coe_adldap['ad_username'];
            $this->ad_password = $coe_adldap['ad_password'];

            $this->_adldap = new adLDAP(array(
                "base_dn" => $this->base_dn,
                "domain_controllers" => $this->domain_controllers,
                "ad_port" => $this->ad_port, // AD port
                "ad_username" => $this->ad_username,
                "ad_password" => $this->ad_password));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * Function to get user details from ldap server 
     * @param type $username
     * @return type
     */
    public function get_user_det($username) {
        $user_info = $this->_adldap->user_info($username);
        //print "<pre>";
        //print_r($user_info);
        return $user_info;
    }

    /**
     * Retrieve user project list from ldap server
     * @param String $username
     * @return Array - list of projects
     */
    public function get_user_project_list($username) {
        global $wpdb;
        $user_info = $this->_adldap->user_info($username);        
        $user_project_list = array();
        if ($user_info) {
            $member_of = $user_info[0]['memberof'];
            unset($member_of['count']);
            foreach ($member_of as $key => $value) {
                if ($value != "")
                    $domain_details = explode(",", $value);
                if ($domain_details[1] == "OU=SVN") {
                    $project_name = str_replace("CN=", "", $domain_details[0]);
                    // Create post for the projects if they do not exist //
                    $postid = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = '" . $project_name . "' and meta_key='project_name' ");
                    if (!$postid) {
                       $post_id = wp_insert_post(
                                array(
                                    'comment_status' => 'closed',
                                    'ping_status' => 'closed',
                                    'post_author' => 1,
                                    'post_title' => $project_name,
                                    'post_name' => $project_name,
                                    'post_status' => 'publish',
                                    'post_type' => 'project'
                                )
                        );
                        add_post_meta($post_id, 'project_name',$project_name );
                        add_post_meta($post_id, 'platform','');
                        add_post_meta($post_id, 'technology_used','');
                        add_post_meta($post_id, 'backend','');
                        $user_project_list[$post_id] = $project_name;
                    } else {
                        $user_project_list[$postid] = $project_name; 
                    }
                }                
            }
        }
        return $user_project_list;
    }

    /**
     * Function to retrieve project team members from ldap server
     * @param String $project_name
     * @return Array - list of projects team members
     */
    public function get_project_team_members($project_name) {
        global $wpdb, $wpdb1;
        $team_members = array();
        $details = array();
        $filter_array = array('objectClass' => 'group', 'cn' => $project_name);
        $base_dn = 'OU=SVN,OU=Groups,DC=payoda,DC=com';
        $attributes = array('member');
        $team_info = $this->search($base_dn, $filter_array, $attributes);
        if (!$team_info) {
            return;
        }
        $team_member_info = $team_info[0]['member'];       
        unset($team_member_info['count']);
        foreach ($team_member_info as $key => $memberinfo) {
            $member_det = explode(',', $memberinfo);
            $team_members[$key] = str_replace("CN=", "", $member_det[0]);            
            $user_info[$key] = $this->_adldap->user_details($team_members[$key]);
            $desig = $user_info[$key][0]['title'][0];           
            if (((substr($desig,0,17))=='Software Engineer') || ((substr($desig,0,25))=='Senior Project Specialist') || ((substr($desig,0,18))=='Project Specialist') || ((substr($desig,0,22))=='Senior Project Partner') || ((substr($desig,0,25))=='Associate Project Partner') || ((substr($desig,0,16))=='Project Partner')){
                $dep = "dev";                
            }
            elseif (((substr($desig,0,24))=='Associate Client Partner') || ((substr($desig,0,14))=='Client Partner') || ((substr($desig,0,26))=='Senior Business Consultant') || ((substr($desig,0,19))=='Business Consultant')){
                $dep = "ba";
            }
            elseif(((substr($desig,0,12))=='UX Architect') || ((substr($desig,0,15))=='Visual Designer')){
                $dep = "ui";
            }
            else {$dep = "qa";}
            $details[$dep][$key]=$user_info[$key][0]['cn'][0];          
           }        
        return $details;
    }

    /**
     * Function to perform search in  ldap server
     * @param String $base_dn - base domain to apply search
     *         Array $filterArray - key/value pair eg : objectCategory => person
     *         Array $attributes - resulting attributes 
     * @return Array - $search_info
     */
    public function search($base_dn, $filterArray = array(), $attributes = array()) {
        if (!$this->customADConnect()) {
            return;
        }
        $filter = $this->makeFilter($filterArray);
        $search_results = ldap_search($this->connection, $base_dn, $filter, $attributes);
        $search_info = ldap_get_entries($this->connection, $search_results);
        
        return $search_info;
    }

    /**
     * Function to frame search string for ldap_search() funtion
     * @param Array $filter - key/value pair eg : objectCategory => person
     * @return String - $filterString
     */
    public function makeFilter($filter = array()) {
        $filterString = '(&';
        foreach ($filter as $key => $value) {
            $filterString .='(' . $key . '=' . $value . ')';
        }
        $filterString .=')';
        return $filterString;
    }

    /**
     * Function to list new joiness of payoda
     * @return Object - $payoda_user
     */
    public function get_new_joinees() {
        if (!$this->customADConnect()) {
            return;
        } else {
            $result = array();
            $search_filter = array('objectCategory' => 'person'); //"(&(objectCategory=person))";
            $attributes = array("whencreated", "title", "displayname");
            $entries = $this->search($this->base_dn, $search_filter, $attributes);
            $payoda_user = array();
            $payoda_user_timestamp = array();
            foreach ($entries as $key => $entry) {
                $payoda_data = new stdClass();
                if ($key != 'count') {
                    $payoda_data->whencreated = $entries[$key]['whencreated'][0];
                    $payoda_data->displayname = $entries[$key]['displayname'][0];
                    $payoda_data->title = $entries[$key]['title'][0];
                    $payoda_user_timestamp = $payoda_data->whencreated;
                    $payoda_user[] = $payoda_data;
                }
            }
            //print_r($payoda_user);

            $payoda_user = (array) $payoda_user;
            foreach ($payoda_user as $key => $node) {
                $timestamps[$key] = $node->whencreated;
            }
            array_multisort($timestamps, SORT_DESC, $payoda_user);
            return $payoda_user;
        }
    }

    public function searchLdapUser($searchKey) {
        $users = array();
        $filter_array = array('objectCategory' => 'person', 'displayname' => '*'.$searchKey.'*');
        $base_dn = 'DC=payoda,DC=com';
        $attributes = array('displayname','title','sAMAccountName');
        $user_info = $this->search($base_dn, $filter_array, $attributes);
        unset($user_info['count']);
        //return $user_info;
        if (!$user_info) {
            return;
        }
        unset($user_info['count']);
        foreach ($user_info as $key=>$value) {
            $userDetails = new stdClass();
            $userDetails->name= $value['displayname'][0];
            $userDetails->title= $value['title'][0];
            $users[$value['samaccountname'][0]]= $userDetails;
        }
       // return $team_members;
       return $users;  
    }
    
    public function searchSvnProject($searchKey) {
        $projects= array();
        $filter_array = array('objectClass' => 'group','cn' => '*'.$searchKey.'*');
        $base_dn = 'OU=SVN,OU=Groups,DC=payoda,DC=com';
        $attributes = array('cn');
        $project_info = $this->search($base_dn, $filter_array, $attributes);
         unset($project_info['count']);
         $project_result= array();
        if(empty($project_info))
        {
            return;
        }
        foreach ($project_info as $key=> $value) {
            $project = new stdClass();
            $project->name = $value['cn'][0];   
            $project->project = 1;
            $project->title = 'SVN';
            $project_result[$project->name]=$project;
        }
        return $project_result;
    }
    /**
     * Function to connect to LDAP server manually. 
     * @return true on success / false on failure
     */
    protected function customADConnect() {
        try {
            $this->connection = ldap_connect($this->domain_controllers[0], $this->ad_port) or die("Could not connect to $this->domain_controllers[0]");
            ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);
            $this->bind = @ldap_bind($this->connection, $this->ad_username, $this->ad_password);
            if (!$this->bind) {
                throw new adLDAPException('Bind to Active Directory failed. Check the login credentials and/or server details');
            }
            return true;
        } catch (adLDAPException $e) {
            define('WP_DEBUG', true);
            error_log($e);
            return;
        }
    }

}

?>
