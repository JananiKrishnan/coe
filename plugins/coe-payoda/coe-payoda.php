<?php
/*
  Plugin Name: COE Payoda
  Plugin URI: http://localhost/wordpress/
  Description: Plugin to COE payoda portal
  Version: 0.1
  Author: Santhiya V,Muhilvarnan V
  Depends: Active Directory Integration,COE Active directory integration
 */

/**
 * COE Active directory plugin integration
 * */
/**
 * Initialize the wordpress press widgets with multiple widget mode
 */
add_action('widgets_init', 'src_load_widgets');

/**
 * Load the widget class
 */
function src_load_widgets() {
    register_widget('Widget_Who_Is_New');
    register_widget('Widget_Project_Team_Members');
    register_widget('Widget_Search');
    register_widget('Widget_Latest_Three_Projects');
}

/**
 * Class for creating widget who is new 
 */
class Widget_Who_Is_New extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Who_Is_New', __('Who is New', 'wpb_widget_domain'), array('description' => __('Display top five members in Payoda Office', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $coeObject = new COECustom();
        $payoda_user = $coeObject->get_new_joinees();
        wp_register_style('newemp-css', plugins_url('newemp/css/component.css', __FILE__));
        wp_enqueue_style('newemp-css');
        wp_register_script('newemp-js', plugins_url('newemp/js/jquery.cbpQTRotator.min.js', __FILE__));
        wp_enqueue_script('newemp-js');
        if (!empty($payoda_user)) {
            ?>
            <div id="cbp-qtrotator" class="cbp-qtrotator" style="border-bottom:solid #339933">
                <?php
                for ($i = 0; $i < 5; $i++) {
                    ?>
                    <div class="cbp-qtcontent">
                        <blockquote>
                            <p>We welcome <b><?php echo $payoda_user[$i]->displayname; ?></b> to payoda family </p>
                            <footer>Designation | <?php echo $payoda_user[$i]->title; ?></footer>
                        </blockquote>
                    </div>
                    <?php
                }
                ?>
            </div>
            <script>
                $(function() {
                    $('#cbp-qtrotator').cbpQTRotator();
                });
            </script>
            <?php
        } else {
            ?>
            Welcome to payoda 
            <?php
        }
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget who is new 
 */
class Widget_Project_Team_Members extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Project_Team_Members', __('COE Project Team Members', 'wpb_widget_domain'), array('description' => __('Display Project team members', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $coeObject = new COECustom();
        $projectTeamMembers = $coeObject->get_project_team_members('PHP-Dizzion');
        //print_r($projectTeamMembers);
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget who is new 
 */
class Widget_Search extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Search', __('COE Search', 'wpb_widget_domain'), array('description' => __('Search projects,Payoda Members and Reuseable compenents in payoda MediaWiki', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        wp_register_style('search-css', plugins_url('search/css/styles.css', __FILE__));
        wp_enqueue_style('search-css');
        ?>
        <div id="search-overlay">
            <h2> Start Search</h2>
            <div id="close">X</div>
            <form>
                        <input id="hidden-search" type="text" autocomplete="off" />

                        <!--hidden input the user types into-->
                        <input id="display-search" type="text"  autocomplete="off" readonly="readonly" />
                        <input id="search_auto_suggest_url" value="<?php echo plugins_url('search/auto-suggest.php', __FILE__) ?>" type="hidden" />
                        <input id="site_base_url" value="<?php echo get_site_url();  ?>" type="hidden" />
                        <!--mirrored input that shows the actual input value-->
                    </form>
              <div id="results" style="width:80%;float:left">
                        <div id="artists" style="width:100%;"></div>
                    </div>
                    <div style="float:right;width:15%;padding: 10px;">
                        <h3>Search Mode</h3>
                        <form class="form-horizontal">
                                    <label class="checkbox">
                                        <input type="checkbox" checked="checked" id="users" name="users"/><span class="metro-checkbox" style="font-size:20px;"><span class="icon-user-3" ></span> - Users</span>
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" checked="checked" id="projects" name="projects" /><span class="metro-checkbox" style="font-size:20px;"><span class="icon-code-2" ></span> - Projects</span>
                                    </label>
                        </form>
                    </div>
                  

                </div>
        <script>
            function BlinkEffect(id, cssProperty, cssValue1, cssValue2) {

            var x;

            setInterval(function() {

                if (x == 0) {

                    $(id).css(cssProperty, cssValue1);

                    x = 1;

                } else {

                    if (x = 1) {

                        $(id).css(cssProperty, cssValue2);

                        x = 0;

                    }

                }

            }, 500);

        }
            $(document).ready(function () {

      BlinkEffect("#display-search", "border-left", "2px solid white", "2px solid #339933");

     //BlinkEffect("#dvSpotLight li a", "color", "lightgray", "black");

}); 
        </script>
        <?php
        
        wp_register_script('search-js', plugins_url('search/scripts/ajax-search-suggest.js', __FILE__));
        wp_enqueue_script('search-js');
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

/**
 * Class for creating widget latest three projects
 */
class Widget_Latest_Three_Projects extends WP_Widget {

    function __construct() {
        parent::__construct('Widget_Latest_Three_Projects', __('Latest Three Projects', 'wpb_widget_domain'), array('description' => __('Display latest three projects in Payoda Office', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        wp_register_style('hovercaption-css', plugins_url('hovercaption/css/component.css', __FILE__));
        wp_enqueue_style('hovercaption-css');
        wp_register_script('hovercaption-js', plugins_url('hovercaption/js/toucheffects.js', __FILE__));
        wp_enqueue_script('hovercaption-js');
        $args = array('posts_per_page' => 5, 'offset' => 1);
        $myposts = get_posts($args);
        if (empty($myposts)) {
            ?>
            <h3>No Projects Created</h3>
            <?php
            return;
        }
        #echo "<pre>";print_r($myposts);exit;
        ?>
        <?php
        if (isset($myposts[0])) {
            ?>
            <div class="pull-left" style="width:68%;height:300px;">
                <ul class="grid cs-style-3" style="float:left">

                    <li>
                        <figure>
                            <?php
                            $imgurl = get_field("project_image", $myposts[0]->ID);
                            if (!isset($imgurl) || $imgurl == "")
                                $img = plugins_url('hovercaption/images/default.png', __FILE__);
                            // $img = get_template_directory_uri() . '/assets/hovercaption/images/4.png';
                            else {
                                $img = $imgurl;
                            }
                            ?>
                            <img src=<?php echo $img ?> style="width:600px;height:300px" alt="img04">
                            <figcaption>

                                <h3><?php echo $myposts[0]->post_title ?></h3>

                            </figcaption>
                        </figure>
                    </li>
                </ul>
            </div>
            <?php
        }
        ?>
        <div style="float:left;width:20%;margin-left:0px;">
            <ul class="grid cs-style-5" style="float:left">
                <?php
                if (isset($myposts[1])) {
                    ?>
                    <li>
                        <figure>
                            <?php
                            $imgurl1 = get_field("project_image", $myposts[1]->ID);
                            $imgurl2 = get_field("project_image", $myposts[2]->ID);
                            if (!isset($imgurl1) || $imgurl1 == "")
                                $img1 = plugins_url('hovercaption/images/default.png', __FILE__);

                            else {
                                $img1 = $imgurl1;
                            }
                            if (!isset($imgurl2) || $imgurl2 == "")
                                $img2 = plugins_url('hovercaption/images/default.png', __FILE__);
                            else {
                                $img2 = $imgurl2;
                            }
                            ?>
                            <img src="<?php echo $img1 ?> "style="width:300px;height:147px" alt="img04">
                            <figcaption>
                                <h3><?php echo $myposts[1]->post_title ?></h3>	
                            </figcaption>
                        </figure>
                    </li>
                    <?php
                }
                if (isset($myposts[2])) {
                    ?>
                    <li>
                        <figure>
                            <img src="<?php echo $img2 ?> " style="width:300px;height:147px" alt="img04">
                            <figcaption>
                                <h3><?php echo $myposts[2]->post_title ?></h3>

                            </figcaption>
                        </figure>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Payoda COE ', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}
?>