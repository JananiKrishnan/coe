<?php

/* * *
 */
//include the array that contains our example search results (in the real world, you'd connect to and query a database)

include('../../../../wp-blog-header.php');
fs_get_wp_config_path();
if (isset($_REQUEST['latestQuery'])) {
    $latestQuery = $_REQUEST['latestQuery'];
    $search = new COECustom();
    $keysearch = array();
    $result = array();
    $projectSearch = array();
    if ($_REQUEST['user']) {
        $keysearch = $search->searchLdapUser($latestQuery);
    }
    if ($_REQUEST['projects']) {
        $projectSearch = $search->searchSvnProject($latestQuery);
        $args = array('s' => $_REQUEST['latestQuery']);
        $search = new WP_Query($args);

        foreach ($search->posts as $key => $post) {

            $category_list = array();
            $categories = get_the_category($post->ID);
            foreach ($categories as $category) {
                if ($category->name != 'Uncategorized' or $category->cat_ID != 1)
                    $category_list[] = $category->name;
            }
            $projectKey = get_post_meta($post->ID, 'project_name', true);
            unset($projectSearch[$projectKey]);
            $category = implode(',', $category_list);
            $result[$projectKey] = array('name' => $post->post_title, 'title' => $category, 'project' => 1,'project_id'=>$post->ID);
        }
    }
    $searchData = array_merge((array)$keysearch,(array)$result);
    $searchData = array_merge((array)$searchData,(array)$projectSearch);
    echo json_encode($searchData);
}

/*
 * Method to return wp_config.php basepath 
 */

function fs_get_wp_config_path() {
    $base = dirname(__FILE__);
    $path = false;

    if (@file_exists(dirname(dirname($base)) . "/wp-config.php")) {
        $path = dirname(dirname($base)) . "/wp-config.php";
    } else
    if (@file_exists(dirname(dirname(dirname($base))) . "/wp-config.php")) {
        $path = dirname(dirname(dirname($base))) . "/wp-config.php";
    } else
        $path = false;

    if ($path != false) {
        $path = str_replace("\\", "/", $path);
    }
    return $path;
}

?>